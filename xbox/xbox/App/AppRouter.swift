//
//  AppRouter.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit

final class AppRouter {
    
    let window: UIWindow
    
    init() {
        window = UIWindow(frame: UIScreen.main.bounds)
    }
    
    func start() {
        self.main()
    }
    
    func main() {
        let viewController = MainNavigation(rootViewController: DiscoverHomeViewController())
        viewController.barTintColor = .primaryColor
        self.window.rootViewController = viewController
        self.window.makeKeyAndVisible()
    }
    
    func onboard() {
        let viewController = OnboardingViewController()
        //viewController.barTintColor = .primaryColor
        self.window.rootViewController = viewController
        self.window.makeKeyAndVisible()
    }
}
