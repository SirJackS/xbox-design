//
//  AppContainer.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import Foundation

let App = AppContainer()

final class AppContainer {
    
    let router = AppRouter()
}
