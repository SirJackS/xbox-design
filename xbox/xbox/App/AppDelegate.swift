//
//  AppDelegate.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        App.router.start()
        return true
    }


}

