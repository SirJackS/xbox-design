//
//  models.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import Foundation

struct Category: Decodable {
    var name: String?
    var icon: String?
}

struct Banner: Decodable {
    var name: String?
    var desc: String?
    var image: String?
}

struct FoodMain: Decodable {
    var name: String?
    var comment: Int?
    var distance: Float?
    var star: Float?
    var image: String?
}

