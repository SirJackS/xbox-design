//
//  SearchView.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

//protocol SearchViewDelegate {
//    func clickSearchField()
//    func searchViewChangedEditing(_ text: String)
//}
//
//extension SearchViewDelegate {
//    func clickSearchField() { }
//    func searchViewChangedEditing(_ text: String) { }
//}


final class SearchView: UIView {

//    var delegate: SearchViewDelegate?
    
//    var isSearchActive: Bool = true
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        //view.layer.masksToBounds = true
        view.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        return view
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.textColor = .white
        textField.font = .systemFont(ofSize: 14, weight: .regular)
        textField.returnKeyType = UIReturnKeyType.search
        textField.tintColor = .white
        textField.placeHolderColor = .white
//        textField.delegate = self
//        textField.addTarget(self, action: #selector(textFieldChangedEditing(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy var searchButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "main-search-icon"), for: .normal)
//        button.image
        return button
    }()
    
    init(placeholder: String) {
        super.init(frame: .zero)
        
        setPlaceholder(placeholder)
        addSubviews()
        setupUI()
    }

    override func updateConstraints() {
        bodyView.anchor(.fillSuperview(), .height(56))
        
        searchButton.anchor(.centerY(), .trailing(-5), .width(46), .height(46))
        
        textField.anchor(.leading(16), .trailing(searchButton.leadingAnchor, constant: -16) ,.centerY())
        
        
        
        super.updateConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(searchButton)
        bodyView.addSubview(textField)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
//        self.layer.cornerRadius = 8
//        self.setupBorder(width: 1, color: UIColor(hexString: "DBDFE3"))
    }
    
    private func setPlaceholder(_ placeholder: String) {
        textField.placeholder = placeholder
        textField.placeHolderColor = UIColor.black.withAlphaComponent(0.4)
    }
}

//extension SearchView: UITextFieldDelegate {
//
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if !isSearchActive {
//            if let delegate = self.delegate {
//                delegate.clickSearchField()
//            }
//            return false
//        }
//        return true
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        guard isSearchActive else { return false }
//
//        if let delegate = self.delegate {
//            let fieldText = textField.text?.trim() ?? ""
//            delegate.searchViewChangedEditing(fieldText)
//        }
//
//        textField.resignFirstResponder()
//        return true
//    }
//
//    @objc func textFieldChangedEditing(_ textField: UITextField) {
//        if let delegate = self.delegate {
//            let fieldText = textField.text?.trim() ?? ""
//            delegate.searchViewChangedEditing(fieldText)
//        }
//    }
//}
