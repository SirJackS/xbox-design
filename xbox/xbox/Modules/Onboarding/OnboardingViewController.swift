//
//  OnboardingViewController.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit

final class OnboardingViewController: UIViewController {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        scrollView.clipsToBounds = false
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var intro1: UIStackView = {
        let view = self.createIntro(
            image: UIImage(named: "intro1")!,
            title: "activate your location, order\n food around you quickly",
            row: 1)
        return view
    }()
    
    private lazy var intro2: UIStackView = {
        let view = self.createIntro(
            image: UIImage(named: "intro1")!,
            title: "500+ store partners, spread\n all over the country",
            row: 2)
        return view
    }()
    
    private lazy var intro3: UIStackView = {
        let view = self.createIntro(
            image: UIImage(named: "intro1")!,
            title: "activate your location, order\n food around you quickly",
            row: 3)
        return view
    }()
    
    private lazy var introStack: UIStackView = {
        let stackView = UIStackView()
        return stackView
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = 3
        pageControl.pageIndicatorTintColor = UIColor(hexString: "BFC6D4")
        pageControl.currentPageIndicatorTintColor = .primaryColor
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Next", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 16, weight: .semibold)
        button.backgroundColor = .primaryColor
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(nextButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var createOneButton: UIButton = {
        let button = UIButton(type: .system)
//        button.setTitle("Next", for: .normal)
//        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 13, weight: .medium)
        button.backgroundColor = .clear
        button.colorStrings(string1: "Don’t have account? ", color1: "171725", string2: "Create one.", color2: "EE6930")
        button.addTarget(self, action: #selector(createOneButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        scrollView.anchor(.top(view.layoutMarginsGuide.topAnchor, constant: 0), .leading(), .trailing())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.heightTo(scrollView))
        
        introStack.anchor(.fillSuperview())
        
        if UIScreen.main.bounds.size.height < 751 {
            pageControl.anchor(
                .top(scrollView.bottomAnchor, constant: 32),
                .centerX())
        } else {
            pageControl.anchor(
                .top(scrollView.bottomAnchor, constant: 56),
                .centerX())
        }
//        pageControl.anchor(
//            .top(scrollView.bottomAnchor, constant: 56),
//            .centerX())
        
        nextButton.anchor(
            .top(pageControl.bottomAnchor, constant: 32),
            .leading(24), .trailing(-24),
            .height(56))
        
        createOneButton.anchor(
            .top(nextButton.bottomAnchor, constant: 16),
            .leading(24), .trailing(-24),
            .height(16),.bottom(view.layoutMarginsGuide.bottomAnchor, constant: -26))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(introStack)
        
        introStack.addArrangedSubview(intro1)
        introStack.addArrangedSubview(intro2)
        introStack.addArrangedSubview(intro3)
        
        self.view.addSubview(pageControl)
        self.view.addSubview(nextButton)
        self.view.addSubview(createOneButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
    }
    
    private func createIntro(image: UIImage, title: String, row: Int) -> UIStackView {
        let contentView = UIView()
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        contentView.addSubview(imageView)
        let screenWidth = UIScreen.main.bounds.size.width
        imageView.anchor(.size(width: screenWidth-48, height: (screenWidth-48)/327*340), .centerSuperview())
        
        let titleLabel = UILabel()
        titleLabel.text = title.capitalized
        titleLabel.setLineSpacing(lineSpacing: 8)
        titleLabel.textColor = UIColor(hexString: "171725")
        titleLabel.font = .systemFont(ofSize: 24, weight: .semibold)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 2
        titleLabel.anchor(.width(self.view.frame.width-48))
        
        let stackView = UIStackView(arrangedSubviews: [contentView, titleLabel])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 40
        stackView.anchor(.width(self.view.frame.width))
        return stackView
    }
    
    private func scrollToPage(_ page: Int, animated: Bool) {
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: animated)
    }

    
    // MARK: - Action
    
    @objc func nextButtonTouchUp() {
        let currentPage = self.pageControl.currentPage
        
        if currentPage == 2 {
            
        }
        else {
            self.scrollToPage(currentPage+1, animated: true)
        }
    }
    
    @objc func createOneButtonTouchUp() {
    }
}


// MARK: - Scroll view delegate

extension OnboardingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
        let isLast = (Int(pageNumber) == 2)
        self.nextButton.setTitle(isLast ? "Start" : "Next", for: .normal)
    }
}

