//
//  DiscoveryHomeFoodCellSecond.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import UIKit

protocol DiscoveryHomeFoodCellSecondDelegate: class {
    func saveButtonClick()
}

final class DiscoveryHomeFoodCellSecond: UICollectionViewCell {
    
    static let ID: String = "DiscoveryHomeFoodCellSecond"
    
    weak var delegate: DiscoveryHomeFoodCellFirstDelegate?
    
    var data: FoodMain! {
        didSet {
            self.configure()
        }
    }
    
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill//.scaleAspectFill
        imageView.anchor(.size(width: self.frame.size.height, height: self.frame.size.height))
        return imageView
    }()
    
    private lazy var gradientView: GradientView = {
        let view = GradientView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.clear
//        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
//    private lazy var gradientLayer: CAGradientLayer = {
//        let gradient = CAGradientLayer()
//        gradient.cornerRadius = 6
//        gradient.colors = [UIColor.red, UIColor.red.withAlphaComponent(0.7)]
//        gradient.locations = [0.0 , 1.0]
//        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
//        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        return gradient
//    }()
//
    
    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "heart-icon"), for: .normal)
        //button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.numberOfLines = 2
        label.font = .systemFont(ofSize: 14, weight: .semibold)
//        label.anchor(.heightLess(32))
        return label
    }()
    
    private lazy var distanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white.withAlphaComponent(0.70)
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var starView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.image = UIImage(named: "star-icon")
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        imageView.anchor(.size(width: 16, height: 16))
        return imageView
    }()
    
    private lazy var starLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var starStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.starView, self.starLabel])
        stackView.alignment = .trailing
        stackView.axis = .horizontal
        stackView.spacing = 6
        return stackView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 8, trailing: -8))
        
        photoView.anchor(.fillSuperview())
        
        gradientView.anchor(.fillSuperview())
        
        starStack.anchor( .bottom(-16), .leading(16), .width(44))
        
        commentLabel.anchor(.leading(starStack.trailingAnchor, constant: 16), .bottom(-16),
                            .trailing(-16))
        
        distanceLabel.anchor(
            .leading(16),
            .bottom(starStack.topAnchor, constant: -24),
            .trailing(-16))
        
        titleLabel.anchor(
            .leading(16),
            .bottom(distanceLabel.topAnchor, constant: -8),
            .trailing(-16))
        
        saveButton.anchor( .top(16), .trailing(-16), .size(width: 16, height: 16))
    
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        
        bodyView.addSubview(gradientView)
    
        gradientView.addSubview(starStack)
        gradientView.addSubview(commentLabel)
        gradientView.addSubview(distanceLabel)
        gradientView.addSubview(titleLabel)
        gradientView.addSubview(saveButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        self.gradientView.setGradientBackground(colorTop: UIColor.black, colorBottom: UIColor.black.withAlphaComponent(0.7))
    }
    
    private func configure() {
        self.photoView.image = UIImage(named: data.image ?? "")
        self.titleLabel.text = data.name
        self.distanceLabel.text = "\(data.distance ?? 0.0) km məsafədə"
        self.commentLabel.text = "\(data.comment ?? 0 ) Rəy"
        self.starLabel.text = "\(data.star ?? 0.0 )"
    }
    
    // MARK: - Action
    
    @objc private func saveButtonTouchUp() {
        self.delegate?.saveButtonClick()
    }
}
