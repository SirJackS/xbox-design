//
//  DiscoveryHomeFoodCell.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

protocol DiscoveryHomeFoodCellFirstDelegate: class {
    func saveButtonClick()
}

final class DiscoveryHomeFoodCellFirst: UICollectionViewCell {
    
    static let ID: String = "DiscoveryHomeFoodCellFirst"
    
    weak var delegate: DiscoveryHomeFoodCellFirstDelegate?
    
    var data: FoodMain! {
        didSet {
            self.configure()
        }
    }
    
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        imageView.anchor(.size(width: self.frame.size.height, height: self.frame.size.height))
        return imageView
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "heart-icon"), for: .normal)
        //button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "171725")
        label.textAlignment = .left
        label.numberOfLines = 2
        label.font = .systemFont(ofSize: 14, weight: .semibold)
        label.anchor(.heightLess(32))
        return label
    }()
    
    private lazy var distanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "929AAB")
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "3E8B69")
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var starView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.image = UIImage(named: "star-icon")
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        imageView.anchor(.size(width: 16, height: 16))
        return imageView
    }()
    
    private lazy var starLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "171725")
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var starStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.starView, self.starLabel])
        stackView.alignment = .trailing
        stackView.axis = .horizontal
        stackView.spacing = 6
        return stackView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 12, trailing: -12))
        
        photoView.anchor(.top(), .leading(), .bottom())
        
        starStack.anchor( .bottom(), .trailing(), .width(44))
        
        commentLabel.anchor(.leading(photoView.trailingAnchor, constant: 16), .bottom(),
                            .trailing(starStack.leadingAnchor, constant: -8))
        
        distanceLabel.anchor(
            .leading(photoView.trailingAnchor, constant: 16),
            .bottom(commentLabel.topAnchor, constant: -16),
            .trailing(starStack.leadingAnchor, constant: -8))
        
        titleLabel.anchor(
            .top(), .leading(photoView.trailingAnchor, constant: 16),
            .bottomGreater(distanceLabel.topAnchor, constant: -6),
            //bura bax
            .trailing(saveButton.leadingAnchor, constant: -6))
        
        saveButton.anchor( .top(2), .trailing(), .size(width: 16, height: 16))
    
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(starStack)
        bodyView.addSubview(commentLabel)
        bodyView.addSubview(distanceLabel)
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(saveButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear

    }
    
    private func configure() {
        self.photoView.image = UIImage(named: data.image ?? "")
        self.titleLabel.text = data.name
        self.distanceLabel.text = "\(data.distance ?? 0.0) km məsafədə"
        self.commentLabel.text = "\(data.comment ?? 0 ) Rəy"
        self.starLabel.text = "\(data.star ?? 0.0 )"
    }
    
    // MARK: - Action
    
    @objc private func saveButtonTouchUp() {
        self.delegate?.saveButtonClick()
    }
}
