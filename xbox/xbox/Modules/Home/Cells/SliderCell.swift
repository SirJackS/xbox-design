//
//  SliderCell.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

final class SliderCell: UICollectionViewCell {
    
    static let ID: String = "SliderCell"
    
    var data: Banner! {
        didSet {
            self.configure()
        }
    }
    
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.backgroundColor = .blue
        return view
    }()
    
    private lazy var gradientView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.70)
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 24, weight: .semibold)
        label.anchor(.height(32))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.anchor(.height(24))
        return label
    }()
    
    private lazy var labelsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.titleLabel, self.descLabel])
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.spacing = 0
        return stackView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 4, trailing: -4))
        
        photoView.anchor(.fillSuperview())
        
        gradientView.anchor(.fillSuperview())
        
        labelsStack.anchor(.leading(20), .trailing(-20), .bottom(-16))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(gradientView)
        bodyView.addSubview(labelsStack)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear

    }
    
    private func configure() {
        self.photoView.image = UIImage(named: data.image ?? "")
        self.titleLabel.text = data.name
        self.descLabel.text = data.desc
    }
}
