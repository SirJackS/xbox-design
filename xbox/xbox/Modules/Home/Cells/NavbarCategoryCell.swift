//
//  NavbarCategoryCell.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

final class NavbarCategoryCell: UICollectionViewCell {
    
    static let ID: String = "NavbarCategoryCell"
    
    var data: Category! {
        didSet {
            self.configure()
        }
    }
    
    
    private lazy var imageBodyView: UIView = {
        let view = UIView(backgroundColor: UIColor.white.withAlphaComponent(0.2))
        view.layer.cornerRadius = self.frame.size.width/2
        view.clipsToBounds = true
        //view.layer.clipsToBounds = true
        return view
    }()
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.anchor(.size(width: (self.frame.size.width/2)-8, height: (self.frame.size.width/2)-8))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 12, weight: .medium)
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        imageBodyView.anchor(.top(), .leading(), .trailing())
        
        iconView.anchor(.centerX(), .centerY())
        
        titleLabel.anchor(
            .top(imageBodyView.bottomAnchor, constant: 6),
            .leading(), .trailing(), .bottom(), .height(14))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(imageBodyView)
        
        imageBodyView.addSubview(iconView)
        self.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configure() {
        self.iconView.image = UIImage(named: data.icon ?? "")
        self.titleLabel.text = data.name
    }
}
