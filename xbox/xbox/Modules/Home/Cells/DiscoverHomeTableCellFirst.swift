//
//  DiscoverHomeTableCellFirst.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

final class DiscoverHomeTableCellFirst: UITableViewCell {

    static let ID: String = "DiscoverHomeTableCellFirst"
    
    var data: [FoodMain] = [] {
        didSet {
            self.foodCollection.reloadData()
        }
    }
    
    private var navbarCategoryHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (88 / 375) * width
    }
    
    private var foodCollectionHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (199 / 375) * width
    }
    
    private lazy var foodCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 23
        layout.itemSize = CGSize.init(width: UIScreen.main.bounds.width * 0.872, height: self.navbarCategoryHeight)
        layout.sectionInset = .init(top: 0, left: 12, bottom: 0, right: 12)
        layout.scrollDirection = .horizontal

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.clipsToBounds = false
        collectionView.isScrollEnabled = true
        collectionView.bounces = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.clipsToBounds = true
        collectionView.register(DiscoveryHomeFoodCellFirst.self, forCellWithReuseIdentifier: DiscoveryHomeFoodCellFirst.ID)
        return collectionView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Nearby"
        label.textColor = UIColor(hexString: "171725")
        label.font = .systemFont(ofSize: 24, weight: .semibold)
        return label
    }()
    
    private lazy var viewAllButton: UIButton = {
        let button = UIButton()
        button.setTitle("View All", for: .normal)
        button.setTitleColor(UIColor(hexString: "929AAB"), for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: .medium)
        button.addTarget(self, action: #selector(viewAllButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        titleLabel.anchor(
            .top(), .leading(24),
            .trailing(viewAllButton.leadingAnchor, constant: -8), .height(32))

        viewAllButton.anchor(.top(8), .trailing(-24),.size(width: 70, height: 16))
        
        foodCollection.anchor(
            .top(titleLabel.bottomAnchor, constant: 24),
            .leading(), .bottom(-30), .trailing(), .height(foodCollectionHeight))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(viewAllButton)
        self.contentView.addSubview(foodCollection)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        self.foodCollection.backgroundColor = .white
        
    }
    
    private func configure() {
//        if let imageUrl = URL(string: data.image ?? "") {
//            self.photoView.sd_setImage(with: imageUrl)
//        }
//        
//        self.titleLabel.text = data.name
//        
//        let ingredients = data.ingredients ?? []
//        let stringIngredients = ingredients.map { $0.name ?? "" }
//        let desc: String = stringIngredients.joined(separator: ", ")
//        self.descLabel.text = desc
//        
//        self.priceLabel.text = "₼ \(data.price ?? "")"
    }
    
    // MARK: - Action
    
    @objc private func viewAllButtonTouchUp() {
        print("alma")
        //self.delegate?.bagButtonClick()
    }
}

// MARK: - UICollectionView datasource and delegate

extension DiscoverHomeTableCellFirst: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case foodCollection:
            return data.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case foodCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DiscoveryHomeFoodCellFirst.ID, for: indexPath) as! DiscoveryHomeFoodCellFirst
            cell.data = data[indexPath.row]
            return cell
            
        default:
            return UICollectionViewCell.init()
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        switch collectionView {
//        case categoryCollection:
//            self.delegate?.didSelectCategory(index: indexPath.row)
//
//        default:
//            break
//        }
//        collectionView.deselectItem(at: indexPath, animated: false)
//    }
}

