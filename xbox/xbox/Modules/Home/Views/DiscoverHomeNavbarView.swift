//
//  DiscoverHomeNavbarView.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

protocol DiscoverHomeNavbarViewDelegate: class {
    func bagButtonClick()
    func searchButtonClick()
}

final class DiscoverHomeNavbarView: UIView {

    weak var delegate: DiscoverHomeNavbarViewDelegate?
    
    lazy var logoView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo-long"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var bagButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "bag-icon"), for: .normal)
        button.tintColor = UIColor.white
        button.anchor(.width(24))
        button.addTarget(self, action: #selector(bagButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "search-icon"), for: .normal)
        button.tintColor = UIColor.white
        button.anchor(.width(24))
        button.addTarget(self, action: #selector(searchButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var buttonsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.bagButton, self.searchButton])
        stackView.spacing = 16
        return stackView
    }()
    
    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        logoView.anchor( .leading(8), .bottom(), .centerY(), .height(24), .width(98))
        
        buttonsStack.anchor(.trailing(-8), .centerY(), .height(24))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width-32
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(logoView)
        addSubview(buttonsStack)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .primaryColor
    }

    
    // MARK: - Action
    
    @objc private func bagButtonTouchUp() {
        self.delegate?.bagButtonClick()
    }
    
    @objc private func searchButtonTouchUp() {
        self.delegate?.searchButtonClick()
    }
}
