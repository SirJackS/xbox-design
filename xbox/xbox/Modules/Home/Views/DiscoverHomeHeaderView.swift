//
//  DiscoverHomeHeaderView.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

protocol DiscoverHomeHeaderDelegate: class {
    func didSelectCategory(index: Int)
}

final class DiscoverHomeHeaderView: UIView {

    weak var delegate: DiscoverHomeHeaderDelegate?
    
    private var indexOfCellBeforeDragging = 0
    
    var categoryList: [Category] = [
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
        Category(name: "Marketlər", icon: "navbar-category-icon"),
    ] {
        didSet {
            self.categoryCollection.reloadData()
        }
    }
    
    var sliderList: [Banner] = [
        Banner(name: "Become a courier", desc: "Courier promotions", image: "banner"),
        Banner(name: "Become a courier", desc: "Courier promotions", image: "banner"),
        Banner(name: "Become a courier", desc: "Courier promotions", image: "banner"),
        Banner(name: "Become a courier", desc: "Courier promotions", image: "banner"),
    ] {
        didSet {
            self.sliderControl.numberOfPages = sliderList.count
            self.sliderCollection.reloadData()
        }
    }
    
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.backgroundColor = .primaryColor
        return view
    }()
    
    lazy var searchView: SearchView = {
        let view = SearchView(placeholder: "What do you need?")
        //view.isSearchActive = false
        return view
    }()
    
    private lazy var sliderCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize.init(width: UIScreen.main.bounds.width * 0.835, height: self.sliderHeight)
        layout.scrollDirection = .horizontal
        layout.sectionInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.isPagingEnabled = false
        collectionView.clipsToBounds = false
        collectionView.bounces = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SliderCell.self, forCellWithReuseIdentifier: SliderCell.ID)
        return collectionView
    }()
    
    private var sliderHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (146 / 375) * width
    }
    
    private var navbarCategoryHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (100 / 375) * width
    }

    private lazy var sliderControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.isUserInteractionEnabled = false
        pageControl.currentPageIndicatorTintColor = UIColor.primaryColor
        pageControl.pageIndicatorTintColor = UIColor(hexString: "BFC6D4")
        //pageControl.anchor(.width(round(CGFloat((sliderList.count-1)) * 12.8 + 6.4)))
        return pageControl
    }()
    
    private lazy var categoryCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize.init(width: UIScreen.main.bounds.width * 0.213, height: self.navbarCategoryHeight)
        layout.sectionInset = .init(top: 0, left: 24, bottom: 0, right: 24)
        layout.scrollDirection = .horizontal

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.clipsToBounds = false
        collectionView.bounces = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.clipsToBounds = true
        collectionView.register(NavbarCategoryCell.self, forCellWithReuseIdentifier: NavbarCategoryCell.ID)
        return collectionView
    }()

    
    init() {
        super.init(frame: .zero)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        topView.anchor(.top(), .leading(), .trailing())
        
        searchView.anchor(.top(24), .leading(24), .trailing(-24))
        
        categoryCollection.anchor(
            .top(searchView.bottomAnchor,constant: 24),
            .leading(), .trailing(),
            .bottom(-24), .height(navbarCategoryHeight))
        
        sliderCollection.anchor(
            .top(topView.bottomAnchor, constant: 16),
            .leading(), .trailing(), .bottom(-47),
            .height(sliderHeight))
        
        sliderControl.anchor(
            .top(sliderCollection.bottomAnchor,constant: 10),
            .leading(24),
            .height(6.4)
        )
        
        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(topView)
        self.addSubview(sliderCollection)
        self.addSubview(sliderControl)
        
        topView.addSubview(searchView)
        topView.addSubview(categoryCollection)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        self.sliderControl.numberOfPages = sliderList.count
    }
    
    //MARK: - Slider Pager
    
    private func indexOfMajorCell() -> Int {
        let itemWidth = UIScreen.main.bounds.width * 0.835
        let proportionalOffset = sliderCollection.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = sliderCollection.numberOfItems(inSection: 0)
        let safeIndex = max(0, min(numberOfItems - 1, index))
        return safeIndex
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if let collectionView = scrollView as? UICollectionView {
            switch collectionView {
            case sliderCollection:
                // Stop scrollView sliding:
                targetContentOffset.pointee = scrollView.contentOffset
                
                // calculate where scrollView should snap to:
                let indexOfMajorCell = self.indexOfMajorCell()
                
                // calculate conditions:
                let dataSourceCount = sliderList.count
                let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
                let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
                let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
                let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
                let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
                
                if didUseSwipeToSkipCell {
                    let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
                    let toValue = UIScreen.main.bounds.width * 0.835 * CGFloat(snapToIndex)

                    // Damping equal 1 => no oscillations => decay animation:
                    UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                        scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                        scrollView.layoutIfNeeded()
                    }, completion: nil)
                } else {
                    let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
                    sliderCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                }
            default: break
            }
        }
        
    }
}

// MARK: - UICollectionView datasource and delegate

extension DiscoverHomeHeaderView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case sliderCollection:
            return sliderList.count
            
        case categoryCollection:
            return categoryList.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case sliderCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderCell.ID, for: indexPath) as! SliderCell
            cell.data = sliderList[indexPath.row]
            return cell
            
        case categoryCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NavbarCategoryCell.ID, for: indexPath) as! NavbarCategoryCell
            cell.data = categoryList[indexPath.row]
            return cell
            
        default:
            return UICollectionViewCell.init()
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        switch collectionView {
//        case categoryCollection:
//            self.delegate?.didSelectCategory(index: indexPath.row)
//
//        default:
//            break
//        }
//        collectionView.deselectItem(at: indexPath, animated: false)
//    }
}

// MARK: - Scroll view delegate

extension DiscoverHomeHeaderView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let collectionView = scrollView as? UICollectionView {
            switch collectionView {
            case sliderCollection:
                let pageNumber = round(scrollView.contentOffset.x / (scrollView.frame.size.width * 0.835))
                self.sliderControl.currentPage = Int(pageNumber)

            default: break
            }
        }
    }
}
