//
//  DiscoverHomeViewController.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit

final class DiscoverHomeViewController: UIViewController {

    
    private lazy var navBarView: DiscoverHomeNavbarView = {
        let view = DiscoverHomeNavbarView()
        view.delegate = self
        return view
    }()
//
//    private lazy var categoryLineView: UIView = {
//        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
//    }()
//
    private lazy var headerView: DiscoverHomeHeaderView = {
        let view = DiscoverHomeHeaderView()
        view.delegate = self
        //view.searchView.delegate = self
        return view
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.register(DiscoverHomeTableCellFirst.self, forCellReuseIdentifier: DiscoverHomeTableCellFirst.ID)
        tableView.register(DiscoverHomeTableCellSecond.self, forCellReuseIdentifier: DiscoverHomeTableCellSecond.ID)
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.estimatedRowHeight = 113
//        tableView.sectionHeaderHeight = 33
//        tableView.sectionFooterHeight = 0
        return tableView
    }()
    
    private var data: [Category] = []
    private var mainFoodData: [FoodMain] = [
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
        FoodMain(name: "Olivier salad with Doctor's Sausage", comment: 1795, distance: 1.4, star: 4.8, image: "food-cell-image"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
//        categorySegmented.anchor(.top(-56), .leading(), .trailing(), .height(56))
//
//        categoryLineView.anchor(.leading(), .trailing(), .bottom(), .height(0.3))
        
        tableView.anchor(.fillSuperview())
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
//        self.view.addSubview(categorySegmented)
//        categorySegmented.addSubview(categoryLineView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.titleView = navBarView
        
        self.tableView.setTableHeaderView(headerView: headerView)
        self.tableView.updateHeaderViewFrame()
    }
    
//    private func setupData(_ data: Restaurant) {
//        self.headerView.categoryList = categories
//        self.headerView.sliderList = data.images ?? []
//    }
    
    
    // MARK: - Action
    
}

extension DiscoverHomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0,2:
            let cell = tableView.dequeueReusableCell(withIdentifier: DiscoverHomeTableCellFirst.ID, for: indexPath) as! DiscoverHomeTableCellFirst
            cell.data = mainFoodData
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: DiscoverHomeTableCellSecond.ID, for: indexPath) as! DiscoverHomeTableCellSecond
            cell.data = mainFoodData
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
}

extension DiscoverHomeViewController: DiscoverHomeNavbarViewDelegate, DiscoverHomeHeaderDelegate {
    func didSelectCategory(index: Int) {
        //
    }
    
    func bagButtonClick() {
        print("qoz")
    }
    
    func searchButtonClick() {
        //
    }
    
    
    
}
