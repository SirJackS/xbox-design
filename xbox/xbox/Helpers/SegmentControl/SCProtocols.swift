//
//  SCProtocols.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 9/1/20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import UIKit

public protocol SegmentedControlDelegate: class {
    func segmentedControl(_ segmentedControl: SegmentedControl, didSelectIndex selectedIndex: Int)
    func segmentedControl(_ segmentedControl: SegmentedControl, didLongPressIndex longPressIndex: Int)
}

public extension SegmentedControlDelegate {
    
    func segmentedControl(_ segmentedControl: SegmentedControl, didSelectIndex selectedIndex: Int) {
    }

    func segmentedControl(_ segmentedControl: SegmentedControl, didLongPressIndex longPressIndex: Int) {
    }
}
