//
//  SCStyle.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 9/1/20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import Foundation

public enum SegmentedControlLayoutPolicy {
    case fixed
    case dynamic
}

public enum SegmentedControlStyle {
    case text
    case image
}

public enum SegmentedControlSelectionIndicatorStyle {
    case none
    case top
    case bottom
}

public enum SegmentedControlSelectionBoxStyle {
    case none
    case `default`
}
