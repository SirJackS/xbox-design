//
//  SCConstants.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 9/1/20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import UIKit

internal struct SelectionIndicator {
    static let defaultHeight: CGFloat = 5
}

internal struct SegmentConstant {
    static let defaultSelectionHorizontalPadding: CGFloat = 15
}
