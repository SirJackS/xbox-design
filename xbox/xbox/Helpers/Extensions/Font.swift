//
//  Font.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 8/14/20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import UIKit

extension UIFont {
    
    enum Font: String {
        case system = "System"
        case avenir = "Avenir"
    }

    enum FontWeight: String {
        case black = "Black"
        case bold = "Bold"
        case book = "Book"
        case italic = "Italic"
        case light = "Light"
        case medium = "Medium"
        case heavy = "Heavy"
        case regular = ""
    }

    convenience init?(ofSize fontSize: CGFloat) {
        self.init(name: .system, weight: .regular, size: fontSize)
    }
    
    convenience init?(name: Font = .system, weight: FontWeight, size: CGFloat) {
        self.init(name: "\(name)-\(weight)", size: size)
    }
   
    convenience init?(_ weight: FontWeight, size: CGFloat) {
        self.init(name: .system, weight: weight, size: size)
    }
}
