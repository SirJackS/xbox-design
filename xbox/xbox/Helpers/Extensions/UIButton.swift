//
//  UIButton.swift
//  xbox
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit

extension UIButton {
    func colorStrings(string1: String = "", color1: String = "000000", string2: String = "", color2: String = "000000") {
        let att = NSMutableAttributedString(string: "\(string1)\(string2)");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: color1) , range: NSRange(location: 0, length: string1.count))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: color2), range: NSRange(location: string1.count, length: string2.count))
        self.setAttributedTitle(att, for: .normal)
    }
}
