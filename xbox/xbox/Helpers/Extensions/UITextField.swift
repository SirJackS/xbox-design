//
//  UITextField.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 03.12.20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import UIKit

extension UITextField {
    
    @objc var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[.foregroundColor: newValue!])
        }
    }
}
