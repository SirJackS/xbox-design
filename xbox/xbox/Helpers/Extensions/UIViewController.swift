//
//  UIViewController.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 8/14/20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import UIKit

extension UIViewController {
    /*
    func setBackButton() {
        let backBtn = UIImage.backIcon.withRenderingMode(.alwaysOriginal)
        self.navigationController?.navigationBar.backIndicatorImage = backBtn
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backBtn
        hideBackTitle()
    }*/

    func hideBackTitle() {
        self.navigationController?.navigationBar.backItem?.title = " "

        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    
    func popVC() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func popToRoot() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func popBack<T: UIViewController>(toControllerType: T.Type) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    
    func showMessage(_ message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alertVC = UIAlertController(title: message, message: "", preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .cancel, handler: handler))
        //alertController.view.tintColor = UIColor.black
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showMessage(_ title: String, _ message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .cancel, handler: handler))
        //alertController.view.tintColor = UIColor.black
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showNetworkError(completion: @escaping (Bool) -> ()) {
        let alertVC = UIAlertController(title: "Oops, something went wrong. Please try again.",
                                        message: "The Internet connection appears to be offline.",
                                        preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            completion(false)
        }))
        alertVC.addAction(UIAlertAction(title: "Try again", style: .default, handler: { (alertAction) in
            completion(true)
        }))
        self.present(alertVC, animated: true)
    }
}
