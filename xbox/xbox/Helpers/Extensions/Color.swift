//
//  Color.swift
//  MenuApp
//
//  Created by Elnur Shahbazov on 8/14/20.
//  Copyright © 2020 Netgroup Team. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hexString: String) {
        
        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner = Scanner(string: hexString as String)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(displayP3Red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: a)
    }

    
    @nonobjc class var contentBg: UIColor {
        return UIColor(red: 249, green: 249, blue: 249)
    }
    
    @nonobjc class var primaryTitle: UIColor {
        return UIColor(red: 68, green: 68, blue: 68)
    }
    
    @nonobjc class var primaryColor: UIColor {
        return UIColor(hexString: "EE6930")
    }
    
}
